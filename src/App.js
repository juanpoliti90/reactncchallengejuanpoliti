import React, { useEffect, useState } from "react";
import "./App.css";
import axios from "axios";
import { makeStyles } from "@material-ui/core/styles";
import {
  Table,
  TableContainer,
  TableHead,
  TableCell,
  TableBody,
  TableRow,
  
} from "@material-ui/core";

function App() {
  const [data, setData] = useState([]);

  const peticionGet = async () => {
    await axios
      .post("http://localhost:8081/auth", {
        username: "sarah",
        password: "connor",
      })
      .then((response) => {
        axios
          .get("http://localhost:8081/api/members", {
            headers: {
              Authorization: `Bearer ${response.data.token}`,
            },
          })
          .then((res) => {
            setData(res.data);
          });
      });
  };

  useEffect(async () => {
    await peticionGet();
  }, []);



  return (
    <div className="App">
      <TableContainer>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell>First Name</TableCell>
              <TableCell>Last Name</TableCell>
              <TableCell>Address</TableCell>
              <TableCell>SSN</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {data.map(data=>(
              <TableRow key={data.id}>
                <TableCell>{data.firstName}</TableCell>
                <TableCell>{data.lastName}</TableCell>
                <TableCell>{data.address}</TableCell>
                <TableCell>{data.ssn}</TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </div>
  );
}

export default App;
