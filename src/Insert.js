import React, { useEffect, useState, useRef } from "react";
import "./App.css";
import axios from "axios";
import {
  Input,
  InputLabel,
  Grid,
  FormControl,
  Button,
} from "@material-ui/core";

export default function Inserter() {
  const [form, setForm] = useState({
    firstName: "",
    lastName: "",
    address: "",
    ssn: "",
  });

  const reset = () => {
    
    setForm({
      firstName: "",
      lastName: "",
      address: "",
      ssn: "",
    });

	console.log(form);
  };

  const handleChange = (e) => {
    const { name, value } = e.target;
    setForm((prevState) => ({
      ...prevState,
      [name]: value,
    }));
    console.log(form);
  };

  const insertRow = async () => {
    if (form.firstName.length < 1) {
      alert("Your first name must be at least one character long");
      return;
    }
    if (form.lastName.length < 1) {
      alert("Your last name must be at least one character long");
      return;
    }
    if (form.address.length < 1) {
      alert("Your address must be at least one character long");
      return;
    }

    await axios
      .post("http://localhost:8081/auth", {
        username: "sarah",
        password: "connor",
      })
      .then((response) => {
        console.log(response.data.token);
        axios.post("http://localhost:8081/api/members", form, {
          headers: {
            Authorization: `Bearer ${response.data.token}`,
          },
        });
      });
  };

  return (
    <Grid id="grid">
      <Grid>
        <FormControl>
          <InputLabel htmlFor="firstName">First Name</InputLabel>
          <Input
            name="firstName"
            id="firstName"
            aria-describedby=""
			value={form.firstName}
            onChange={handleChange}
          />
        </FormControl>
      </Grid>
      <Grid>
        <FormControl>
          <InputLabel htmlFor="lastName">Last Name</InputLabel>
          <Input
            name="lastName"
            id="lastName"
            aria-describedby=""
			value={form.lastName}
            onChange={handleChange}
          />
        </FormControl>
      </Grid>
      <Grid>
        <FormControl>
          <InputLabel htmlFor="address">Address</InputLabel>
          <Input
            name="address"
            id="address"
            aria-describedby=""
			value={form.address}
            onChange={handleChange}
          />
        </FormControl>
      </Grid>
      <Grid>
        <FormControl>
          <InputLabel>SSN</InputLabel>
          <Input
            name="ssn"
            id="ssn"
            aria-describedby=""
			value={form.ssn}
            onChange={handleChange}
          />
        </FormControl>
      </Grid>
      <Button variant="contained" onClick={insertRow}>
        Save
      </Button>
      <Button variant="contained" onClick={reset}>
        Reset
      </Button>
    </Grid>
  );
}


