import * as React from 'react';
import {
   AppBar,
   Box,
   Toolbar,
   IconButton,
   Typography,
   Button,
  } from "@material-ui/core";


export default function ButtonAppBar() {
  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar position="static">
        <Toolbar>
          <IconButton
            size="large"
            edge="start"
            color="inherit"
            aria-label="menu"
            sx={{ mr: 2 }}
          >
            
          </IconButton>
          <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
            App React
          </Typography>
          <Button color="inherit" href="/">Home</Button>
          <Button color="inherit" href="/insert" >Insertar</Button>
        </Toolbar>
      </AppBar>
    </Box>
  );
}